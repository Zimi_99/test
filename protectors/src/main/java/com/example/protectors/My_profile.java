package com.example.protectors;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.frenclub.json.RegisterUserResponse;
import com.frenclub.json.UpdateProfileResponse;

/**
 * Created by apollo on 4/30/2015.
 */
public class My_profile extends android.support.v4.app.Fragment {

    EditText mail, user, password, confirm_password,nick_name;
    String email, username, userpass, con_userpass, avatar,mynick;
    int gender=1;
    Button send;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.my_profile, container, false);
        mail = (EditText) rootview.findViewById(R.id.email);
       // username = (EditText) rootview.findViewById(R.id.username);
        password = (EditText) rootview.findViewById(R.id.password);
        confirm_password = (EditText) rootview.findViewById(R.id.confirm_password);
        nick_name= (EditText) rootview.findViewById(R.id.nickname);

        Button change_arm = (Button) rootview.findViewById(R.id.change_armour);
        change_arm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Armour.class);
                startActivity(intent);
            }
        });


        send = (Button) rootview.findViewById(R.id.send_myprofile);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = mail.getText() + "";
                username = mail.getText() + "";//user.getText() + "";
                userpass = password.getText() + "";
                con_userpass = confirm_password.getText() + "";
                mynick=nick_name.getText()+"";

                new testUpdProfile(getActivity(), username, userpass, gender, Splash_Activity.tok, Login.pid, avatar,mynick).execute();

            }
        });


        return rootview;

    }

    class testUpdProfile extends AsyncTask<String, Void, UpdateProfileResponse> {
        Context mcontext;
        String muser, mpass;
        int mtype, mpid;
        String mtoken;
        String mavatar, mnick;

        public testUpdProfile(Context context, String user, String password, int genderType, String tok, int pid, String avatar, String nickname) {
            this.mcontext = context;
            this.muser = user;
            this.mpass = password;
            this.mtype = genderType;
            this.mtoken = tok;
            this.mpid = pid;
            this.mavatar = avatar;
            this.mnick = nickname;
        }


        @Override
        protected UpdateProfileResponse doInBackground(String... params) {


            return ServiceHandler.testUpdProfile(muser,mavatar,mtype,mnick,mpid,mpass,mtoken,mnick);
        }

        @Override
        protected void onPostExecute(UpdateProfileResponse result) {
            super.onPostExecute(result);
            if (result.getResult() == 1) {
                Toast.makeText(getActivity(), "profile updated", Toast.LENGTH_LONG).show();
            }

        }


    }

}