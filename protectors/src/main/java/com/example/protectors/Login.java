package com.example.protectors;

import java.security.PublicKey;

import android.R.string;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.protectors.utilities.Preferences;
import com.frenclub.json.SignInResponse;

import org.json.JSONArray;

public class Login extends ActionBarActivity {

    EditText email;
    EditText password;
    Button login;
    public static int pid;
    int login_type = 0;
    String user,pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        email = (EditText) findViewById(R.id.email_txt);
        password = (EditText) findViewById(R.id.Password_txt);


        Button sign_up = (Button) findViewById(R.id.singup);
        sign_up.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent signup_intent = new Intent(Login.this, Register.class);
                startActivity(signup_intent);
            }
        });


        Button forgetpass = (Button) findViewById(R.id.forgotpassword);
        forgetpass.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent forgetpass_intent = new Intent(Login.this, Forget_password.class);
                startActivity(forgetpass_intent);


            }
        });


        login = (Button) findViewById(R.id.user_log);
        login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String e_mail = email.getText() + "";
                final String upassword = password.getText() + "";
                if (e_mail.length() == 0 || upassword.length() == 0) {
                    AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();
                    alertDialog.setTitle("Message");
                    alertDialog.setMessage(" Invalid Email or Password ");
                    alertDialog.setButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //dismiss the dialog
                                }
                            });
                    alertDialog.show();
                }

//            }
//
//
                new testSignIn(Login.this, e_mail, upassword, login_type, Splash_Activity.tok).execute();


            }


        });


    }

    class testSignIn extends AsyncTask<String, Void, SignInResponse> {
        Context mcontext;
        String memail, mpass;
        int mtype;
        String mtoken;


        public testSignIn(Context context, String email, String password, int login_type, String tok) {
            this.mcontext = context;
            this.memail = email;
            this.mpass = password;
            this.mtype = login_type;
            this.mtoken = tok;

        }


        @Override
        protected SignInResponse doInBackground(String... params) {


            return ServiceHandler.testSignIn(memail, mpass, mtype, mtoken);
        }

        @Override
        protected void onPostExecute(SignInResponse result) {
            super.onPostExecute(result);
            if (result.getResult() == 1) {
                pid = result.getPid();
                Preferences.savePID(mcontext, pid);
                pid = Preferences.getPID(mcontext);
               // Log.d("" + Preferences.getPID(mcontext),"");

                Intent intent = new Intent(Login.this, Main_fragment.class);
                startActivity(intent);
                user=result.getUsername();
                Preferences.saveuser(mcontext, user);



            }

        }
    }

}
