package com.example.protectors;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.*;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;
import android.widget.Toast;

import com.example.protectors.utilities.Preferences;
import com.frenclub.json.FirstRunResponse;

import org.json.JSONArray;

import java.net.URL;


public class Splash_Activity extends Activity {

    public String version;
    public long lat, lon;
    public String androidID;
    int deivce_type = 2;
    public static String tok;
    public static int iaid;

    private static String url = "http://protectorshq.com/protectors/v1.3/index.php";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spash);
        version = Build.VERSION.RELEASE;
        //Toast.makeText(this, version.toString(), Toast.LENGTH_LONG).show();
        androidID = Settings.System.getString(this.getContentResolver(), Settings.System.ANDROID_ID);
        // Toast.makeText(this, "" + androidID, Toast.LENGTH_LONG).show();
        //Toast.makeText(this, "device type is " + deivce_type, Toast.LENGTH_LONG).show();

        GPSService mGPSService = new GPSService(this);
        mGPSService.getLocation();


        lat = Math.round(mGPSService.getLatitude());//GPSService.mLatitude;
        lon = Math.round(mGPSService.getLongitude());//GPSService.mLongitude;
        String test = Preferences.getToken(Splash_Activity.this);
        if (test == null) {
            new FristRun(this, androidID, version, lat, lon, deivce_type).execute();
        }


        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(5000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    //Intent intent =  new Intent(Splash_Activity.this, Tutorial_main.class);
                    startActivity(new Intent(Splash_Activity.this, Tutorial_main.class));
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }


    class FristRun extends AsyncTask<String, Void, FirstRunResponse> {

        Context mcontext;
        String mandroidID;
        String mversion;
        long mlat;
        long mlon;
        int mdeivce_type;

        public FristRun(Context context, String androidID, String mversion, long lat, long lon, int deivce_type) {
            this.mcontext = context;
            this.mandroidID = androidID;
            this.mversion = mversion;
            this.mlat = lat;
            this.mlon = lon;
            this.mdeivce_type = deivce_type;

        }

        @Override
        protected FirstRunResponse doInBackground(String... params) {

            return ServiceHandler.testFirstRun(mandroidID, mdeivce_type, mversion, mlat, mlon);

        }

        @Override
        protected void onPostExecute(FirstRunResponse result) {
            super.onPostExecute(result);
            if (result.getResult() == 1) {
                tok = result.getToken().toString();
                Preferences.saveToken(mcontext, tok);
                String test = Preferences.getToken(mcontext);


                Log.d("asda" + test, "yhuji");
                iaid = result.getIaid();
            }
        }
    }


}



