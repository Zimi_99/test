package com.example.protectors;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.protectors.R;
import com.example.protectors.utilities.Preferences;
import com.frenclub.json.RegisterUserResponse;
import com.frenclub.json.SignInResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class Register extends ActionBarActivity {

    private EditText user_mail;
    private EditText user_passsword;
    private Button confirm;
    private EditText confirm_pass;
    HttpClient httpClient;
    HttpPost httpPost;
    String usermail, password, conpass;
    RadioGroup gender;
    int genderType=1;
    int pid;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        user_mail = (EditText) findViewById(R.id.myemail);
        user_passsword = (EditText) findViewById(R.id.mypass);
        confirm = (Button) findViewById(R.id.confirm);
        confirm_pass = (EditText) findViewById(R.id.txtPasswordConfirm);

        gender= (RadioGroup) findViewById(R.id.genderGroup);
        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(checkedId == R.id.female)
                {
                    genderType = 2;
                }
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usermail = user_mail.getText() + "";
                password = user_passsword.getText() + "";
                conpass = confirm_pass.getText() + "";
                new testreg(Register.this,usermail,password, genderType, Splash_Activity.tok,Splash_Activity.iaid).execute();


//                if (usermail.length() == || password.length() == 0 || conpass.length() == 0) {
//                    AlertDialog alertDialog = new AlertDialog.Builder(Register.this).create();
//                    alertDialog.setTitle("Message");
//                    alertDialog.setMessage(" E-main address or Password is missing ");
//                    alertDialog.setButton("Ok",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    //dismiss the dialog
//                                }
//                            });
//
//                    alertDialog.show();
//                }
//                if (password.equals(conpass)) {
//                    Toast.makeText(Register.this, "passwords are matched ", Toast.LENGTH_LONG).show();
//
//                } else {
//                    AlertDialog alertDialog = new AlertDialog.Builder(Register.this).create();
//                    alertDialog.setTitle("Message");
//                    alertDialog.setMessage("Password do not matched");
//                    alertDialog.setButton("Ok",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    //dismiss the dialog
//                                }
//                            });
//
//                    alertDialog.show();
//                }
//
//
//
//



            }
        });


    }

    class testreg extends AsyncTask<String, Void, RegisterUserResponse> {
        Context mcontext;
        String memail, mpass;
        int mtype,mid;
        String mtoken;


        public testreg(Context context, String email, String password, int genderType, String tok,int iadi) {
            this.mcontext = context;
            this.memail = email;
            this.mpass = password;
            this.mtype = genderType;
            this.mtoken = tok;
            this.mid=iadi;
        }


        @Override
        protected RegisterUserResponse doInBackground(String... params) {


            return ServiceHandler.testRegUser(memail,mpass,mid, mtype, mtoken);
        }

        @Override
        protected void onPostExecute(RegisterUserResponse result) {
            super.onPostExecute(result);
            if (result.getResult() == 1) {

                pid = result.getPid();
                Preferences.savePID(mcontext, pid);
                pid = Preferences.getPID(mcontext);
                Intent intent = new Intent(Register.this, Main_fragment.class);
                startActivity(intent);
            }

        }
    }




}
