package com.example.protectors;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by apollo on 4/13/2015.
 */
public class Neighbor extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rooView = inflater.inflate(R.layout.neighbor, container, false);

        Button crtnn = (Button) rooView.findViewById(R.id.crtnewneighbor);
        crtnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),NeighbourGrp.class);
                startActivity(intent);
            }
        });

        Button defult_grp = (Button) rooView.findViewById(R.id.default_grp);

        defult_grp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.neighbour, new Chatroom()).addToBackStack("tag").commit();
            }
        });


        return rooView;
    }
}
