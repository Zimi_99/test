package com.example.protectors;

import com.astuetz.PagerSlidingTabStrip;
import com.example.protectors.TabsPagerAdapter;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.AbsListView;

public class Main_fragment extends FragmentActivity implements
        ActionBar.TabListener {

    private ViewPager viewPager;
    //private TabsPagerAdapter mAdapter;
    // private ActionBar actionBar;
    // Tab titles
    //private String[] tabs = { "Home", " Alerts", "Spot Check", "Setting",
    //	"current location" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // actionBar=getActionBar();
        // actionBar.setHomeButtonEnabled(false);
        // actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        // for (String tab_name : tabs) {
        // actionBar.addTab(actionBar.newTab().setText(tab_name)
        // .setTabListener(this));
        // }

        setContentView(R.layout.main_fragment);

        // Initilization
        viewPager = (ViewPager) findViewById(R.id.pager);
        //mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        //viewPager.setAdapter(mAdapter);

        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setShouldExpand(false);
        tabs.setViewPager(viewPager);

    }


    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        // on tab selected
        // show respected fragment view
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
    }


}
