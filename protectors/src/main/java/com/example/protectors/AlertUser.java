package com.example.protectors;

/**
 * Created by ASUS on 5/26/2015.
 */
public class AlertUser {
    String name;
    String  imagesId;
    String description;
    public AlertUser(String name,  String description, String imagesId)
    {
        super();
        this.name= name;
        this.description = description;
        this.imagesId = imagesId;


    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagesId() {
        return imagesId;
    }

    public void setImagesId(String imagesId) {
        this.imagesId = imagesId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
