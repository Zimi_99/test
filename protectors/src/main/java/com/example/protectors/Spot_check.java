package com.example.protectors;

import com.example.protectors.R;
import com.google.android.gms.maps.SupportMapFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Spot_check extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.map_main, container, false);
        FragmentTransaction mTransaction = getFragmentManager()
                .beginTransaction();
        SupportMapFragment mFRaFragment = new MapFragmentD();
        mTransaction.add(R.id.mainl, mFRaFragment);
        mTransaction.commit();
        return rootView;
    }
}
