package com.example.protectors;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.widget.Button;

class TestFragmentAdapter extends FragmentPagerAdapter  {
    protected static final String[] CONTENT = new String[] { "", "", "", "", "",""};
    private int[] offerImages = {
			R.drawable.page,
			R.drawable.page2,
			R.drawable.page3,
			R.drawable.page4,
			R.drawable.page5,
			R.drawable.page5_1,
	};
    private int mCount = CONTENT.length;

    public TestFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new TestFragment(offerImages[position]);
        
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return TestFragmentAdapter.CONTENT[position % CONTENT.length];
    }

   
    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}