package com.example.protectors;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class Settings_help extends  Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView =inflater.inflate(R.layout.setting_help, container, false);
		
		Button ranking=(Button) rootView.findViewById(R.id.ranking);
		ranking.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				FragmentManager fm=getFragmentManager();
				FragmentTransaction ft=fm.beginTransaction();
				//Ranking llf=new Ranking();
				//ft.replace(R.id.settings_help, llf);
				//ft.commit();
				ft.replace( R.id.settings, new Ranking() ).addToBackStack( "tag" ).commit();

			}
		});
		
		
		Button tutorial=(Button) rootView.findViewById(R.id.stg_tutorial);
		
		tutorial.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(getActivity(),Tutorial_main.class);
				startActivity(intent);
			}
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		return rootView;
		
		
		
	}

	
	
	
}
