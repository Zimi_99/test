package com.example.protectors.utilities;

import android.content.Context;
import android.content.SharedPreferences;


public class Preferences {

    public static void saveToken(Context context, String atoken) {

        SharedPreferences userInfo = getSharedPreferences(context);
        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString("Token", atoken);
        editor.commit();
    }

    public static String getToken(Context context) {
        SharedPreferences userInfo = getSharedPreferences(context);
        return userInfo.getString("Token", null);
    }

    public static void saveuser(Context context, String auser) {

        SharedPreferences userInfo = getSharedPreferences(context);
        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString("use", auser);
        editor.commit();
    }

    public static String getuser(Context context) {
        SharedPreferences userInfo = getSharedPreferences(context);
        return userInfo.getString("use", null);
    }

    public static void savePID(Context context, int pid) {

        SharedPreferences userInfo = getSharedPreferences(context);
        SharedPreferences.Editor editor = userInfo.edit();
        editor.putInt("PID", pid);
        editor.commit();
    }
    public static int getPID(Context context) {
        SharedPreferences userInfo = getSharedPreferences(context);
        return userInfo.getInt("PID",0);
    }


    public static void saveCurrentLocation(Context context, double latitude, double longitude) {
        SharedPreferences userInfo = getSharedPreferences(context);
        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString("saveLat", latitude + "");
        editor.putString("saveLon", longitude + "");
        editor.commit();
    }

    public static void saveEmail(Context context, String email) {
        SharedPreferences userInfo = getSharedPreferences(context);
        SharedPreferences.Editor editor = userInfo.edit();
        editor.putString("saveEmail", email);
        editor.commit();
    }


    public static String getEmail(Context context) {
        SharedPreferences userInfo = getSharedPreferences(context);
        return userInfo.getString("getEmail", null);
    }



    public static void clearUserInfo(Context context) {
        SharedPreferences userInfo = getSharedPreferences(context);
        SharedPreferences.Editor editor = userInfo.edit();
        editor.clear();
        editor.commit();
    }


    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("getSharedPreferences",Context.MODE_PRIVATE); //( Context.MODE_PRIVATE);
    }
}
