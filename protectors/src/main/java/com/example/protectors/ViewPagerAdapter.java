package com.example.protectors;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.astuetz.PagerSlidingTabStrip;
//import com.google.android.gms.internal.po;

public class ViewPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
    private int pageCount = 5;

    private int[]
            ICONS = new int[]{R.drawable.new15, R.drawable.neighbor15,
            R.drawable.share15, R.drawable.spotchk15, R.drawable.setting15};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getPageIconResId(int position) {
        // TODO Auto-generated method stub
        return ICONS[position];
    }

    @Override
    public Fragment getItem(int index) {
        switch (index) {
            case 0:
                return new Alerts();
            case 1:
                return new Neighbor();
            case 2:
                return new Main();
            case 3:
                return new Spot_check();
            case 4:
                return new Settings();


            default:
                return null;
        }
    }




    @Override
    public int getCount() {
        return pageCount;
    }
}