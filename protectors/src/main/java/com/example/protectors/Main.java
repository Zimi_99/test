package com.example.protectors;

import android.app.Dialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Main extends Fragment {

    Button crime_btn;
    Button summon_btn;
    Button hazard_btn;
    Button help_btn;
    Button check_in;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.main, container, false);

        crime_btn = (Button) rootView.findViewById(R.id.crime);
        crime_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.main, new Crime()).addToBackStack("tag").commit();

            }
        });

        summon_btn = (Button) rootView.findViewById(R.id.summon);

        summon_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.main, new Summon()).addToBackStack("tag").commit();

            }
        });

        hazard_btn = (Button) rootView.findViewById(R.id.hazard);

        hazard_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.main, new Hazard()).addToBackStack("tag").commit();

            }
        });


        help_btn = (Button) rootView.findViewById(R.id.help);

        help_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(getActivity());
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View customView = inflater.inflate(R.layout.help, null);
                dialog.setContentView(customView);
                final TextView mTextField = (TextView) customView.findViewById(R.id.timer);


                // dialog.setTitle("You are calling for help");
                final Button dialogbutton = (Button) customView.findViewById(R.id.cancel);
                dialogbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


                dialog.show();

                new CountDownTimer(6000, 1000) {
                    @Override

                    public void onTick(long millisUntilFinished) {
                        mTextField.setText("" + millisUntilFinished / 1000);
                        // Log.d("timer", "timer");
                    }

                    @Override
                    public void onFinish() {
                        dialog.dismiss();
                    }
                }.start();


            }
        });

        return rootView;

    }
}
