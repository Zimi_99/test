package com.example.protectors;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

public class Hazard extends Fragment {

    ImageButton natural_btn, manmade, util, animal;


    Button mcam;
    private static final int CAMERA_PIC_REQUEST = 1337;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootview = inflater.inflate(R.layout.hazard, container, false);

        natural_btn = (ImageButton) rootview.findViewById(R.id.naturalImg);

        manmade = (ImageButton) rootview.findViewById(R.id.roadImg);
        util = (ImageButton) rootview.findViewById(R.id.utilityImg);

        animal = (ImageButton) rootview.findViewById(R.id.animalImg);

        natural_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.hazard_xml, new Natural()).addToBackStack("tag").commit();
            }
        });

        manmade.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.hazard_xml, new ManMade()).addToBackStack("tag").commit();
            }
        });


        util.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.hazard_xml, new Utility()).addToBackStack("tag").commit();
            }
        });


        animal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fr = getFragmentManager();
                FragmentTransaction ft = fr.beginTransaction();
                ft.replace(R.id.hazard_xml, new Animal()).addToBackStack("tag").commit();
            }
        });


        mcam = (Button) rootview.findViewById(R.id.uploadPhoto);
        mcam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CamAct.class);
                startActivity(intent);


            }
        });


        Button map = (Button) rootview.findViewById(R.id.viewMap);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Map_main.class);
                startActivity(intent);
            }
        });

        Button cal_hazard= (Button) rootview.findViewById(R.id.date);
        cal_hazard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });



        return rootview;
    }


}
