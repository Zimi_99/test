package com.example.protectors;

import com.example.protectors.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;

public class Settings extends Fragment {
    Button contect_us;
    Button term_of_use;
    Button help_setting;
    Button my_profile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.settings, container, false);


        contect_us = (Button) rootView.findViewById(R.id.contect_us);
        contect_us.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.settings, new Contect_us()).addToBackStack("tag").commit();


            }
        });

        help_setting = (Button) rootView.findViewById(R.id.button5);
        help_setting.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.settings, new Settings_help()).addToBackStack("tag").commit();

            }
        });


        term_of_use = (Button) rootView.findViewById(R.id.button4);
        term_of_use.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.settings, new Terms()).addToBackStack("tag").commit();

            }
        });
        my_profile = (Button) rootView.findViewById(R.id.myprofile);
        my_profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.settings, new My_profile()).addToBackStack("tag").commit();

            }
        });

        return rootView;


    }
}
