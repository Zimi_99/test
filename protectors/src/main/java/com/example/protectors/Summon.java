package com.example.protectors;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

public class Summon extends Fragment {

    Button mcam;
    private static final int CAMERA_PIC_REQUEST = 1337;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootview = inflater.inflate(R.layout.summon, container, false);

        mcam = (Button) rootview.findViewById(R.id.uploadPhoto);
        mcam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CamAct.class);
                startActivity(intent);


            }
        });


        Button map = (Button) rootview.findViewById(R.id.viewMap);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Map_main.class);
                startActivity(intent);
            }
        });

        Button cal_summon= (Button) rootview.findViewById(R.id.date);
        cal_summon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });
        ImageButton police= (ImageButton) rootview.findViewById(R.id.policeImg);
        police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ImageButton localgov= (ImageButton) rootview.findViewById(R.id.localgvImg);
        localgov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });







        return rootview;
    }

}
