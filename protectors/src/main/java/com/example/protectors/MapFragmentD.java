package com.example.protectors;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapFragmentD extends SupportMapFragment implements LocationListener{

	GoogleMap mapView;
	protected LocationManager mLocationManager;
	Location mLocation;
	double mLatitude;
	double mLongitude;
	Context mContext;
	static String address = "";
	
	private static final long TIME = 30000;
	private static final long DISTANCE = 20;
	
	LocationManager lm;

	@Override
	public void onCreate(Bundle arg0) {
		super.onCreate(arg0);
	}

	@Override
	public View onCreateView(LayoutInflater mInflater, ViewGroup arg1,
			Bundle arg2) {
		return super.onCreateView(mInflater, arg1, arg2);
	}

	@Override
	public void onInflate(Activity arg0, AttributeSet arg1, Bundle arg2) {
		super.onInflate(arg0, arg1, arg2);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		
/*		mLocation = mLocationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);*/
		
//		LocationManager lm = (LocationManager)mContext.getSystemService(mContext.LOCATION_SERVICE);
//		Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
//		last working 
/*		LocationManager mgr = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
		Location location = mgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		mLatitude = location.getLatitude();
		mLongitude = location.getLongitude();*/
		
		//test

		GPSService mGPSService = new GPSService(getActivity());
		mGPSService.getLocation();

		address = mGPSService.getLocationAddress();
		
		mapView = getMap();
		MarkerOptions markerOptions = new MarkerOptions().title(address);
		markerOptions.draggable(true);
	//	markerOptions.position(new LatLng(23.231251f, 71.648437f));
		markerOptions.position(new LatLng(mGPSService.getLatitude(),mGPSService.getLongitude()));
		markerOptions.icon(BitmapDescriptorFactory.defaultMarker());
		mapView.addMarker(markerOptions);
		mapView.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mGPSService.getLatitude(),mGPSService.getLongitude()), 17));
		
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
