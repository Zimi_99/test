package com.example.protectors;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Login_home_page extends Activity {
    private final String TAG = getClass().getName();
    public Button skipbtn;
    Button loginButton;
    Button logorsing;

    private CallbackManager callbackManager;


    private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }

    private PendingAction pendingAction = PendingAction.NONE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.protectors",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));

                // Toast.makeText(this, "key hash " +  Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_LONG);
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();


        setContentView(R.layout.login_home_page);
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        //  Toast.makeText(Login_home_page.this, "login successful " + loginResult.getAccessToken(), Toast.LENGTH_LONG).show();
                        updateUI();

                    }

                    @Override
                    public void onCancel() {
                        if (pendingAction != PendingAction.NONE) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (pendingAction != PendingAction.NONE
                                && exception instanceof FacebookAuthorizationException) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                    }

                    private void showAlert() {
                        new AlertDialog.Builder(Login_home_page.this)
                                .setTitle("Cancelled")
                                .setMessage("permission not granted")
                                .setPositiveButton("OK", null)
                                .show();
                    }
                });

        findViewById(R.id.fblogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginManager.getInstance() != null) {
                    try {
                        Log.d("==>", "called on click");

                        LoginManager.getInstance().logInWithReadPermissions(Login_home_page.this, Arrays.asList("public_profile", "user_friends", "email"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        skipbtn = (Button) findViewById(R.id.skip);
        skipbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(Login_home_page.this, Remind.class);
                startActivity(intent);
            }
        });

        logorsing = (Button) findViewById(R.id.login_or_singup);
        logorsing.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ///Toast.makeText(Login_home_page.this, "THIS IS A TEST LOGIN", 200).show();
                Intent intent = new Intent(Login_home_page.this, Login.class);
                startActivity(intent);

            }
        });
    }

    private void updateUI() {
        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;


        Profile profile = Profile.getCurrentProfile();

        if (enableButtons && profile != null) {
            Toast.makeText(Login_home_page.this, " user name " + profile.getFirstName(), Toast.LENGTH_SHORT).show();
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject user, GraphResponse response) {
                    if (user != null) {
                        try {
                            Log.d("user info ", "" + user.toString(3));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            Bundle params = request.getParameters();
            params.putString("fields", "email, name");
            request.setParameters(params);

            request.executeAsync();

        } else {
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "activity result handled");
        callbackManager.onActivityResult(requestCode, resultCode, data);

        Intent myintent = new Intent(Login_home_page.this, Main_fragment.class);
        startActivity(myintent);

        Toast.makeText(Login_home_page.this, "valid facebook user", Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);


    }




}


