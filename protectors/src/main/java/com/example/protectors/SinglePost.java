package com.example.protectors;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by apollo on 5/28/2015.
 */

public class SinglePost extends Activity {
    Button map, comment, share;
    String value,desa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_post);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
             value = extras.getString("name");
             desa = extras.getString("des");
        }

        TextView name= (TextView) findViewById(R.id.textView2);
        name.setText(value);
        TextView des= (TextView) findViewById(R.id.textView6);
        des.setText(desa);

        share = (Button) findViewById(R.id.share_single);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.hello_world)));

            }
        });


    }
}
