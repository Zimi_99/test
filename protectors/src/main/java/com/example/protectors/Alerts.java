package com.example.protectors;

import com.example.protectors.R;
import com.example.protectors.utilities.Preferences;
import com.frenclub.json.SelectRecentResponse;
import com.frenclub.json.SignInResponse;
//import com.squareup.picasso.Picasso;
//import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.util.ArrayList;

public class Alerts extends Fragment {
    ListView myListView;
    ImageView myImageView;
    ArrayList<AlertUser> userArrayList = new ArrayList<>();
    AlertCustomAdapter userArrayAdapter;
    int eid;
    String name;
    String pic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.alerts, container, false);
//
//        userArrayList.add(new AlertUser("Rahim", "Should I use A, AN or THE? Even after years of studying English, students may find themselves asking that question over and over again. English articles can be quite a challenge. To help every English learner become an articles expert, Englishpage.com has put together the most comprehensive " +
//                " English articles tutorial on the web.", R.drawable.icon));
//        userArrayList.add(new AlertUser("Karim", "Should I use A, AN or THE? Even after years of studying English, students may find themselves asking that question over " +
//                "and over again. English articles can be quite a challenge. To help every English " +
//                "learner become an articles expert, Englishpage.com has put together the most comprehensive" +
//                " English articles tutorial on the web.", R.drawable.icon));
//        userArrayList.add(new AlertUser("Fahad", "Should I use A, AN or THE? Even after years of studying English, students may find themselves asking that question over " +
//                "and over again. English articles can be quite a challenge. To help every English " +
//                "learner become an articles expert, Englishpage.com has put together the most comprehensive" +
//                " English articles tutorial on the web.", R.drawable.icon));
//        userArrayList.add(new AlertUser("Shahariar", "Should I use A, AN or THE? Even after years of studying English, students may find themselves asking that question over " +
//                "and over again. English articles can be quite a challenge. To help every English " +
//                "learner become an articles expert, Englishpage.com has put together the most comprehensive" +
//                " English articles tutorial on the web.", R.drawable.icon));
//        userArrayList.add(new AlertUser("Babul", "Should I use A, AN or THE? Even after years of studying English, students may find themselves asking that question over " +
//                "and over again. English articles can be quite a challenge. To help every English " +
//                "learner become an articles expert, Englishpage.com has put together the most comprehensive" +
//                " English articles tutorial on the web.", R.drawable.icon));
//        userArrayList.add(new AlertUser("zimi", "Should I use A, AN or THE? Even after years of studying English, students may find themselves asking that question over " +
//                "and over again. English articles can be quite a challenge. To help every English " +
//                "learner become an articles expert, Englishpage.com has put together the most comprehensive" +
//                " English articles tutorial on the web.", R.drawable.icon));

        userArrayList.add(new AlertUser("Please waith a while", "This page is refreshing", "Loading"));



        userArrayAdapter = new AlertCustomAdapter(getActivity(), R.layout.alert_program_list, userArrayList);
        myListView = (ListView) rootView.findViewById(R.id.listView);
        myListView.setItemsCanFocus(false);
        myListView.setAdapter(userArrayAdapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Toast.makeText(getActivity(),
                        "List Item Clicked:" + position, Toast.LENGTH_LONG)
                        .show();
            }
        });


        new testSelectRecent(getActivity(), Preferences.getPID(getActivity()), eid, Preferences.getToken(getActivity())).execute();

        return rootView;


    }

    class testSelectRecent extends AsyncTask<String, Void, SelectRecentResponse> {
        Context mcontext;
        int mpid, meid;
        String mtoken;


        public testSelectRecent(Context context, int apid, int aeid, String tok) {
            this.mcontext = context;
            this.mpid = apid;
            this.meid = aeid;
            this.mtoken = tok;

        }

        @Override
        protected SelectRecentResponse doInBackground(String... params) {


            return ServiceHandler.testSelectRecent(mpid, meid, mtoken);
        }

        @Override
        protected void onPostExecute(SelectRecentResponse result) {
            super.onPostExecute(result);
            if (result.getResult() == 1) {

                // pid = result.getPid();
                // name=result.getJSON().getString("state");




               // pid = result.getPid();
                userArrayList.clear();
                JSONArray alartArray = result.getList();
                for(int i = 0; i<alartArray.length(); i++) {
                    try {
                        name= alartArray.getJSONObject(i).getString("message");
                        userArrayList.add(new AlertUser(alartArray.getJSONObject(i).getString("name"), alartArray.getJSONObject(i).getString("message"), alartArray.getJSONObject(i).getString("image")));
                        userArrayAdapter.notifyDataSetChanged();
                        Log.i("check","name" + name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(),"Sorry for the error, Please try again",Toast.LENGTH_LONG).show();
                    }
                }

            }

        }
    }

}








