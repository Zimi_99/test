package com.example.protectors;

import com.example.protectors.R;
import com.example.protectors.R.id;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Current_location extends Fragment {
 public TextView address_view;
 ImageButton chk;
 
 
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.current_location, container, false);
		address_view=(TextView) rootView.findViewById(R.id.txtLocation);
		
		chk=(ImageButton) rootView.findViewById(R.id.checkin);
		chk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				GPSService mGPSService = new GPSService(getActivity());
		        mGPSService.getLocation();

		        String tvAddress = mGPSService.getLocationAddress();				
				address_view.setText(tvAddress.toString());	
							
			}
		});
		
		
		
		
		ImageButton mapview=(ImageButton) rootView.findViewById(R.id.view_map_location);
		mapview.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			Intent intent=new Intent(getActivity(),Map_main.class);
			 startActivity(intent);
				
				
			}
		});
		
		
		return rootView;
	}
}
