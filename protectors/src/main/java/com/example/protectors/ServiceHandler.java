package com.example.protectors;

import com.frenclub.json.FcsApi;
import com.frenclub.json.FirstRunRequest;
import com.frenclub.json.FirstRunResponse;
import com.frenclub.json.RegisterUserRequest;
import com.frenclub.json.RegisterUserResponse;
import com.frenclub.json.SelectRecentRequest;
import com.frenclub.json.SelectRecentResponse;
import com.frenclub.json.SignInRequest;
import com.frenclub.json.SignInResponse;
import com.frenclub.json.UpdateProfileRequest;
import com.frenclub.json.UpdateProfileResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class ServiceHandler {

    static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
    public static String url = "http://protectorshq.com/protectors/v1.3/index.php";


    public ServiceHandler() {

    }

    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }

    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     */
    public String makeServiceCall(String url, int method,
                                  List<NameValuePair> params) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            // Checking http request method type
            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                // adding post params
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }

                httpResponse = httpClient.execute(httpPost);

            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);

                httpResponse = httpClient.execute(httpGet);

            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    }

    public synchronized static FirstRunResponse testFirstRun(String deviceid, int devicetype, String version, long devicelan, long decivelot) {
        FirstRunRequest req = new FirstRunRequest();
        FirstRunResponse rsp = null;

        //Default
        req.setDeviceid(deviceid);
        req.setDevicetype(devicetype);
        req.setLat(decivelot);
        req.setLon(devicelan);
        req.setOsversion(version);


        FcsApi fapi = new FcsApi();
        fapi.setUrl(url);
        fapi.setaToken("testing123");
        rsp = fapi.performFirstRun(req);
        return rsp;

        //System.out.println(rsp.getJSON());
    }

    public synchronized static SignInResponse testSignIn(String mail, String password, int type, String token) {
        SignInRequest req = new SignInRequest();
        SignInResponse rsp = null;

        //Default
        req.setUsername(mail);
        req.setPassword(password);
        req.setLogintype(type);


        //OPTIONAL
        //req.setFbid("");


        FcsApi fapi = new FcsApi();
        fapi.setUrl(url);
        fapi.setaToken(token);
        rsp = fapi.performSignIn(req);
        return rsp;
        // System.out.println(rsp.getJSON());
    }

    public synchronized static RegisterUserResponse testRegUser(String mail, String password, int installid, int gender, String token) {
        RegisterUserRequest req = new RegisterUserRequest();
        RegisterUserResponse rsp = null;

        //Default
        req.setGender(gender);
        req.setIaid(installid);
        req.setPassword(password);
        req.setUsername(mail);
        req.setFbid("");


        //OPTIONAL
        //req.setFbid();
        //req.setLogintype();

        FcsApi fapi = new FcsApi();
        fapi.setUrl(url);
        fapi.setaToken(token);
        rsp = fapi.performRegUser(req);
        return rsp;
        //System.out.println(rsp.getJSON());
    }

    public synchronized static UpdateProfileResponse testUpdProfile(String username, String avatar, int gender, String nickname, int pid, String password, String token, String mnick) {

        UpdateProfileRequest req = new UpdateProfileRequest();
        UpdateProfileResponse rsp = null;

        //Default
        req.setAvatar("http://test.com/1.jpg");
        req.setGender(gender);
        req.setUsername(username);
        req.setNickname(nickname);
        req.setPid(pid);
        req.setPassword(password);
        FcsApi fapi = new FcsApi();
        fapi.setUrl(url);
        fapi.setaToken(token);
        rsp = fapi.performUpdProfile(req);
        return rsp;

        // System.out.println(rsp.getJSON());
    }

    public synchronized static SelectRecentResponse testSelectRecent(int pid ,int eid , String token) {
        SelectRecentRequest req = new SelectRecentRequest();
        SelectRecentResponse rsp = null;

        //Default
        req.setEid(50);
        req.setPid(pid);


        FcsApi fapi = new FcsApi();
        fapi.setUrl(url);
        fapi.setaToken(token);
        rsp = fapi.performSelectReecent(req);
        return rsp;
        //System.out.println(rsp.getJSON());
        //System.out.println(rsp.getString());
    }


}