package com.example.protectors;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by apollo on 5/5/2015.
 */
public class Chatroom extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View rootview=inflater.inflate(R.layout.chat_room,container,false);
        return rootview;
    }
}
