package com.example.protectors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

//import com.squareup.picasso.Picasso;

import com.example.protectors.utilities.AppConstant;

import java.util.ArrayList;

/**
 * Created by ASUS on 5/26/2015.
 */
public class AlertCustomAdapter extends ArrayAdapter {

    Context context;
    int layoutResourceId;
    ArrayList<AlertUser> users = new ArrayList<>();
    String imagePath;


    public AlertCustomAdapter(Context context, int layoutResourceId, ArrayList<AlertUser> users) {
        super(context, layoutResourceId, users);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.users = users;

    }

    public View getView(int position, View V, ViewGroup parent) {
        View item = V;

        UserField viewHolder = null;

        if (item == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            item = inflater.inflate(layoutResourceId, parent, false);
            viewHolder = new UserField();
            viewHolder.name = (TextView) item.findViewById(R.id.username);
            viewHolder.description = (TextView) item.findViewById(R.id.description);
            viewHolder.myimage = (ImageView) item.findViewById(R.id.userimage);


            viewHolder.comment = (Button) item.findViewById(R.id.commentbutton);
            viewHolder.share = (Button) item.findViewById(R.id.sharebutton);

            item.setTag(viewHolder);
        } else {
            viewHolder = (UserField) item.getTag();
        }

        final AlertUser user = users.get(position);
        viewHolder.name.setText(user.getName());
        viewHolder.description.setText(user.getDescription());
        //  viewHolder.myimage.setImageResource(user.getImagesId());


 if (user.getImagesId().contains("upload")) {
            String upload = user.getImagesId();
            imagePath = AppConstant.url.concat(upload);
            Log.i("check", "url: " + imagePath);
/*            Picasso.with(context)
                    .load(url)
                    .resize(100, 100)
                    .into(viewHolder.myimage);*/
        }

        viewHolder.comment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You click in Comment button...!!!", Toast.LENGTH_LONG).show();
            }
        });

        viewHolder.share.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.hello_world)));
            }
        });

        viewHolder.description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SinglePost.class);
                intent.putExtra("name",user.name);
                intent.putExtra("des",user.description);
                context.startActivity(intent);


            }
        });


        return item;

    }

    public class UserField {
        TextView name;
        TextView description;
        ImageView myimage;
        Button comment;
        Button share;
    }
}
