package com.example.protectors;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.nfc.tech.NfcBarcode;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.SupportMapFragment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.zip.Inflater;

public class Crime extends Fragment {

    ImageButton mcam;
    ImageButton cal;
    private static final int CAMERA_PIC_REQUEST = 1337;
    private Uri imageuri;
    ImageButton map;
    public String tvAddress;
    ImageButton suspect;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    ImageButton crime;

    public View onCreateView(final LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootview = inflater.inflate(R.layout.crime, container, false);
        mcam = (ImageButton) rootview.findViewById(R.id.uploadPhoto);
        mcam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CamAct.class);
                startActivity(intent);
//                final CharSequence[] items = { "Take Photo", "Choose from Library",
//                        "Cancel" };
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                builder.setTitle("Add Photo!");
//                builder.setItems(items, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int item) {
//                        if (items[item].equals("Take Photo")) {
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            startActivityForResult(intent, REQUEST_CAMERA);
//                        } else if (items[item].equals("Choose from Library")) {
//                            Intent intent = new Intent(
//                                    Intent.ACTION_PICK,
//                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                            intent.setType("image/*");
//                            startActivityForResult(
//                                    Intent.createChooser(intent, "Select File"),
//                                    SELECT_FILE);
//                        } else if (items[item].equals("Cancel")) {
//                            dialog.dismiss();
//                        }
//                    }
//                });
//                builder.show();

            }
        });


        cal = (ImageButton) rootview.findViewById(R.id.date);
        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");


            }
        });

        map = (ImageButton) rootview.findViewById(R.id.viewMap);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Map_main.class);
                startActivity(intent);


            }
        });


        suspect = (ImageButton) rootview.findViewById(R.id.susactImg);
        suspect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        crime= (ImageButton) rootview.findViewById(R.id.crimeImg);
        crime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });


        return rootview;


    }
}