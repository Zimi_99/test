package com.example.protectors;

import com.astuetz.PagerSlidingTabStrip;
import com.example.protectors.Alerts;
import com.example.protectors.Spot_check;
import com.example.protectors.Main;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;

public class TabsPagerAdapter extends FragmentPagerAdapter {
    private String tabtitles[] = new String[]{"Alert Page", "Spot ", "Scene", "Setting"};
    Context context;
    boolean isContentLoaded = false;


    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new Alerts();
            case 1:
                return new Spot_check();
            case 2:
                return new Main();
            case 3:
                return new Settings();
//		case 4:
//			return new Current_location();
//		case 5:
//			return new Contect_us();
//		case 6:
//			return new Settings_help();
//		case 7:
//			return new Ranking();
        }

        return null;
    }

    @Override
    public CharSequence getPageTitle(int page) {
        // TODO Auto-generated method stub
        return tabtitles[page];
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }


}
