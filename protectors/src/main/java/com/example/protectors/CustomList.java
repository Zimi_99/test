package com.example.protectors;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.protectors.R;

public class CustomList extends ArrayAdapter<String>{

    private final Activity context;
    //private final String[] web;
    private final Integer[] imageId;
    public CustomList(Activity context,
                      String[] image_no, Integer[] imageId) {
        super(context, R.layout.avatar_image, image_no);
        this.context = context;
        this.imageId = imageId;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.avatar_image, null, true);
        //TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.avatarimage);
        //txtTitle.setText(web[position]);

        imageView.setImageResource(imageId[position]);
        return rowView;
    }
}